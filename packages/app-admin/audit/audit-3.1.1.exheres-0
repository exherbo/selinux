# Copyright 2016 Julien Pivotto <roidelapluie@inuits.eu>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libpeas-1.18.0.exheres-0', which is:
#   Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>

require systemd-service

SUMMARY="User space tools for kernel auditing"
DESCRIPTION="
The audit package contains the user space utilities for storing and searching the audit records
generate by the audit subsystem in the Linux 2.6 kernel.
"
HOMEPAGE="https://people.redhat.com/sgrubb/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"

PYTHON2_SUP="2.7"
PYTHON3_SUP="3.8 3.9 3.10"

MYOPTIONS="
    libcap-ng
    python
    python? (
        python_abis: ( ${PYTHON2_SUP} ${PYTHON3_SUP} ) [[ number-selected = at-least-one ]]
        python_abis: ( ${PYTHON2_SUP} ) [[ number-selected = at-most-one ]]
        python_abis: ( ${PYTHON3_SUP} ) [[ number-selected = at-most-one ]]
    )
"

python_dependencies() {
    for so in ${PYTHON2_SUP}; do
        echo "python_abis:${so}? ( dev-lang/python:${so} )"
    done
    for so in ${PYTHON3_SUP}; do
        echo "python_abis:${so}? ( dev-lang/python:${so} )"
    done
}

DEPENDENCIES="
    build:
        dev-lang/swig
        sys-kernel/linux-headers[>=2.6.30]
    build+run:
        net-directory/openldap
        libcap-ng? ( sys-libs/libcap-ng )
        python? (
            $(python_dependencies)
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-listener
    --enable-systemd
    --enable-zos-remote
    --disable-gssapi-krb5
    --disable-static
    --without-apparmor
    --without-golang
    --without-libwrap
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    libcap-ng
)

DEFAULT_SRC_COMPILE_PARAMS=( CC_FOR_BUILD="$(exhost --build)-cc" )

DEFAULT_SRC_INSTALL_PARAMS=( initdir="${SYSTEMDSYSTEMUNITDIR}" )

src_prepare() {
    default

    edo sed \
        -e 's:use_libwrap = yes:use_libwrap = no:g' \
        -i init.d/auditd.conf
}

src_configure(){
    PYTHON_OPTIONS=()
    if optionq python; then
        for so in ${PYTHON2_SUP}; do
            optionq python_abis:${so} && PYTHON2_SO=${so}
        done
        if [[ -n ${PYTHON2_SO} ]]; then
            PYTHON_OPTIONS+=(
                '--with-python'
                "PYTHON=/usr/$(exhost --target)/bin/python${PYTHON2_SO}"
            )
        else
            PYTHON_OPTIONS+=( '--without-python' )
        fi

        for so in ${PYTHON3_SUP}; do
            optionq python_abis:${so} && PYTHON3_SO=${so}
        done
        if [[ -n ${PYTHON3_SO} ]]; then
            PYTHON_OPTIONS+=(
                '--with-python3'
                "PYTHON3=/usr/$(exhost --target)/bin/python${PYTHON3_SO}"
            )
        else
            PYTHON_OPTIONS+=( '--without-python3' )
        fi
    else
        PYTHON_OPTIONS+=( '--without-python3' '--without-python' )
    fi

    local myconf=(
        "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_WITHS[@]}" ; do \
            option_with ${s} ; \
        done )
    )

    econf \
        "${myconf[@]}" \
        "${PYTHON_OPTIONS[@]}"
}

src_install() {
    default

    keepdir /etc/audit/rules.d
    keepdir /var/log/audit
}

